<!--
    可用的圖片 icon class 名稱: fa fa-users, fa fa-laptop, fa fa-cogs, fa-tasks, fa fa-line-chart
-->
<div id="sidebar" class="sidebar responsive">
    <ul class="nav nav-list">
        <!-- 首頁 -->
        <li id="main" class="">
            <a href="index.php">
                <i class="menu-icon fa"></i>
                登入
            </a>
            <b class="arrow"></b>
        </li>

        <!-- 功能1 -->
        <li id="template_id" class="">
            <a href="page_template.php">
                <i class="menu-icon fa fa-bar-chart"></i>
                頁面功能名稱1
            </a>
            <b class="arrow"></b>
        </li>

        <!-- 功能2 -->
        <li id="modaal_test" class="">
            <a href="page_modaal.php">
                <i class="menu-icon fa fa-laptop"></i>
                Modaal 測試
            </a>
            <b class="arrow"></b>
        </li>

        <!-- JsTree -->
        <li id="jstree_test" class="">
            <a href="page_jstree.php">
                <i class="menu-icon fa fa-file-archive-o"></i>
                JsTree 測試
            </a>
            <b class="arrow"></b>
        </li>

        <!-- 兩層選單 -->
        <li id="software-type" class="">
            <a href="javascript:;" class="dropdown-toggle">
                <i class="menu-icon fa fa-file-image-o"></i>
                <span class="menu-text"> 功能分類 </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <!-- 清除資產用途工具 -->
                <li id="software-type-management" class="">
                    <a href="page_software_type_management.php">
                        <i class="menu-icon fa"></i>
                        功能分類1
                    </a>
                    <b class="arrow"></b>
                </li>
                <li id="software-type-report" class="">
                    <a href="page_software_type_report.php">
                        <i class="menu-icon fa"></i>
                        功能分類2
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

    </ul><!-- /.nav-list -->
</div>
