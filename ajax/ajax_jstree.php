<?php
	require __DIR__."/../vendor/autoload.php" ;

    $log = new \Henwen\Log\ClassErrorHandler("test.log") ;

    $id = intval($_GET["file_id"]) ;

    $log->manualMessageHandler($id) ;

    $tree = array() ;

    if ($id === 1) {
        $tree[] = array(
            "id" => "2",
            "text" => "目錄 1",
            "state" => array(
                "opened" => false
            ),
            "children" => true,
            "path" => "\\ip\\Log\\dir1"
        ) ;
        $tree[] = array(
            "id" => "3",
            "text" => "目錄 2",
            "state" => array(
                "opened" => false
            ),
            "children" => false,
            "path" => "\\ip\\Log\\dir2"
        ) ;
        $tree[] = array(
            "id" => "4",
            "text" => "目錄 3",
            "state" => array(
                "opened" => false
            ),
            "children" => false,
            "path" => "\\ip\\Log\\dir3"
        ) ;
    }

    else if ($id === 2) {
        $tree[] = array(
            "id" => "5",
            "text" => "目錄 4",
            "state" => array(
                "opened" => false
            ),
            "children" => false,
            "path" => "\\ip\\Log\\dir4"
        ) ;
        $tree[] = array(
            "id" => "6",
            "text" => "目錄 5",
            "state" => array(
                "opened" => false
            ),
            "children" => false,
            "path" => "\\ip\\Log\\dir5"
        ) ;
    }

    echo json_encode($tree) ;
    exit ;
