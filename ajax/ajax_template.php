<?php
	require __DIR__."/../vendor/autoload.php" ;

	// $obj = new Henwen\Project\ClassTemplate();

	// 錯誤輸出時，讓 JqGrid 返回時彈出錯誤訊息，並停止程式。
	$error = array(
		"msg" => "Error Message"
	) ;

	$result = array(
		"rows" => array(
			array(
				"col_1" => "test1",
				"col_2" => "test2",
				"col_3" => "test3",
				"col_4_text" => "Link",
				"col_4_href" => "http://www.yahoo.com.tw"
			),
			array(
				"col_1" => "test1",
				"col_2" => "test2",
				"col_3" => "test3",
				"col_4_text" => "Link",
				"col_4_href" => "http://www.yahoo.com.tw"
			),
		),
		"page" => 1,
		"total" => 10,
		"records" => 300,
	) ;

	// echo json_encode($error, JSON_UNESCAPED_UNICODE) ;
	// exit ;
	echo json_encode($result, JSON_UNESCAPED_UNICODE) ;
	exit ;