<?php
	require __DIR__."/../vendor/autoload.php" ;

	//---------- 註冊：PHP Error Handler & Meekrodb Log ----------//
    \Henwen\Log\ClassError::phpErrorHandlerRegister() ;
    \Henwen\Log\ClassError::meekrodbHandlerRegister("test.log") ;
    //-------------------------------------------------------//

	// code ...

	//---------- 復原：Error Handler ----------//
    \Henwen\Log\ClassError::phpErrorHandlerRestore() ;
    \Henwen\Log\ClassError::meekrodbHandlerRestore() ;
    //----------------------------------------//

    exit ;
