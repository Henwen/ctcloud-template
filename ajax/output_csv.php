<?php
	require __DIR__."/../vendor/autoload.php" ;

	$filename = "report_test" ;

	$data = array(
		array("a" => 1, "b" => 2, "c" => 3, "d" => 4),
		array("a" => 5, "b" => 6, "c" => 7, "d" => 8)
	) ;

	$title = array("first", "second", "third") ;

	$column = array("a", "c", "d") ;

	// 輸出全部
	// Henwen\Project\ClassCommon::outputCSV($filename, $data) ;

	// 輸出特定欄位
	Henwen\Common\ClassCommon::outputCSV($filename, $data, $title, $column) ;
