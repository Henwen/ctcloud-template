<?php
    // require_once("verify.php") ;
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>頁面 Template</title>
    <!-- bootstrap & fontawesome -->
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="bower_components/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <!-- page specific plugin styles -->

    <!-- ace styles -->
    <link rel="stylesheet" href="resources/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style">
    <link rel="stylesheet" href="resources/css/ace-skins.min.css" class="ace-main-stylesheet">

    <!-- page specific plugin styles -->
	<link rel="stylesheet" href="resources/assets/css/jquery-ui.css" />
	<link rel="stylesheet" href="resources/assets/css/datepicker.css" />
	<link rel="stylesheet" href="resources/assets/css/ui.jqgrid.css" />
    <!-- ace styles -->
	<link rel="stylesheet" href="resources/assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

    <!--[if lte IE 9]>
      <link rel="stylesheet" href="resources/css/ace-part2.min.css" class="ace-main-stylesheet" />
    <![endif]-->
    <!--[if lte IE 9]>
      <link rel="stylesheet" href="resources/css/ace-ie.min.css" />
    <![endif]-->
    <!-- inline styles related to this page -->
    <!-- ace settings handler -->

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
    <!--[if lte IE 8]>
    <script src="resources/js/html5shiv.min.js"></script>
    <script src="resources/js/respond.min.js"></script>
    <![endif]-->
</head>
<body class="skin-1">
    <!-- #section: header.php -->
    <?php
        // 側邊選單
        require_once('header.php') ;
    ?>
    <!-- #section: header.php -->

    <div class="main-container" id="main-container">
        <!-- #section:basics/sidebar -->
        <?php
            // 側邊選單
            require_once('sidebar.php') ;
        ?>
        <!-- /section:basics/sidebar -->

        <div class="main-content">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->
                    <div class="page-header">
                        <h1>這邊是頁面主題</h1><h5>頁面細節敘述</h5>
                        <br />
                        <button type="button" class="btn btn-danger" data-toggle="collapse" data-target="#tutor"><i class="fa fa-question-circle"></i> 說明</button>
                        <div id="tutor" class="alert alert-success collapse" role="alert">
                            功能說明<br />
                            1. 這裡可以放功能說明。<br />
                            2. 第二個功能說明
                        </div>
                        <button id="btn-download" class="btn btn-info"/><i class="ace-icon fa fa-cloud-download"></i> 下載報表</button>
                    </div>

                    <div class="row">
                        <div class="col-md-10" id="input-data">
                            <p>
                                <label>起訖日期 </label>
                                <input id="start_date" type="date" value="<?php echo date("Y-m-01") ; ?>" />
                                <label> 至 </label>
                                <input id="end_date" type="date" value="<?php echo date("Y-m-d") ; ?>" />
                                <label> 動作 </label>
                                <select id="act">
                                    <option value="r">讀取</option>
                                    <option value="d">刪除</option>
                                </select>
                                <label> 組織代碼 </label> <input id="group_no" type="text" placeholder="請輸入組織代碼" />
                                <button id="btn-search" class="btn btn-success"/><i class="ace-icon fa fa-plus-square-o"></i> 查詢</button>
                            </p>
                        </div> <!-- End col-md-8 -->
                    </div> <!-- End class row -->

                    <p>
                        <ul class="nav nav-tabs">
                            <li role="presentation" class="active"><a href="#">Bootstrap Nav 1</a></li>
                            <li role="presentation"><a href="#">Bootstrap Nav 2</a></li>
                        </ul>
                    </p>

                    <table id="grid-table"></table>
                    <div id="grid-pager"></div>

                    <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div><!-- /.main-content -->

        <?php
            require_once("footer.php") ;
        ?>
    </div><!-- /.main-container -->

    <!-- basic scripts -->`
    <!--[if !IE]> -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <!-- <![endif]-->
    <!--[if IE]>
    <script src ="resources/js/jquery1x.min.js"></script>
    <![endif]-->
    <!-- page specific plugin scripts -->
    <script src="resources/assets/js/jqGrid/jquery.jqGrid.src.js"></script>
    <script src="resources/assets/js/jqGrid/i18n/grid.locale-en.js"></script>

    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- page specific plugin scripts -->
    <!-- ace scripts -->
    <script src="resources/js/ace-elements.min.js"></script>
    <script src="resources/js/ace.min.js"></script>
    <!-- 自訂 -->
    <script src="js/template.js"></script>
</body>
<script type="text/javascript">
    $(document).ready(function(){
        // JQuery Code here
        // 表單箭頭特效
        $("#template_id").addClass("active") ;
    }) ;
</script>
</html>
