<div class="footer">
    <div class="footer-inner">
        <!-- #section:basics/footer -->
        <div class="footer-content">
            <p>WinNexus Server <span>  ver. 2.1.1423</span></p>
            Copyright © 2010 WinNexus Team. All Rights Reserved.<br>
        </div>
        <!-- /section:basics/footer -->
    </div>
</div><!-- END footer -->
