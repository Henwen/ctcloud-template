<?php
    session_start() ;

    // 重建 session id
    session_regenerate_id() ;

    // 驗證是否已經登入：account:存在, user_id:整數
    if (empty($_SESSION["account"]) || ! is_numeric(intval($_SESSION["user_id"])) ) {
        header("Location: index.php") ;
        exit ;
    }
?>
