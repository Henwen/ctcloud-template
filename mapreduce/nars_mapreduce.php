<?php
    require_once __DIR__."/../vendor/autoload.php" ;

    // --------------- Map-Reduce PHP CLI 參數--------------- //

    if (count($argv) < 2) {
        echo "參數不足\n" ;
        echo "使用方式:\n" ;
        echo "php nars_mapreduce [all | yesterday | range] {start_date} {end_date}\n" ;
        echo "all: 全部資料做 Map-Reduce\n" ;
        echo "day: 對昨日資料做 Map-Reduce\n" ;
        echo "range: 對特定日期範圍做 Map-Reduce\n" ;
        exit ;
    }
    // 執行類型：{all | day | range}
    $type = $argv[1] ;

    // all
    if ($type === "all") {
        echo "對全部資料做 Map-Reduce \n" ;
    }

    // yesterday
    else if ($type === "yesterday") {
        // 印出執行日期範圍訊息
        $start_date = date("Y-m-d", strtotime("-1 day")) ;
        echo "對日期 $start_date 做 Map-Reduce \n" ;
        // 起始與終點 Unix Timestamp
        $s_ut = strtotime("-1 day") ;
        $e_ut = strtotime("-1 day") + 86400 ;
    }

    // range (start, end)
    else if ($type === "range") {
        if (count($argv) !== 4) {
            echo "參數不足：缺少日期範圍\n" ;
            exit ;
        }
        // 印出執行日期範圍訊息
        $start_date = $argv[2] ;
        $end_date = $argv[3] ;
        echo "對特定日期範圍 ($start_date, $end_date) 做 Map-Reduce \n" ;
        // 起始與終點 Unix Timestamp
        $s_ut = strtotime($start_date) ;
        $e_ut = strtotime($end_date) ;
        // 起始日期不能大於終點日期
        if (! $s_ut || ! $e_ut) {
            echo "日期字串不合法\n" ;
            exit ;
        }
        if ($s_ut > $e_ut) {
            echo "起始日期不能大於終點日期\n" ;
            exit ;
        }
        $e_ut += 86400 ;
    }

    else {
        echo "參數不正確\n" ;
        exit ;
    }

    // --------------- Map-Reduce --------------- //

    // 設定 Map function, Reduce function
    $map = 'function() {
        var now = new Date(this.time * 1000) ;
        var today_start = new Date(now.getFullYear(), now.getMonth(), now.getDate())/1000 ;
        var act_r = (! this.act) ? 1 : 0 ;
        var act_d = (this.act) ? 1 : 0 ;
        emit( {d : today_start, u : this.user_id, f : this.path}, {r : act_r, d : act_d} ) ;
    }' ;
    $reduce = 'function(key, vals){
        var data = {r : 0, d : 0} ;
        for (var i in vals) {
            data.r += vals[i].r ;
            data.d += vals[i].d ;
        }
        return data ;
    }' ;

    // 1. 初始化
    $obj = new \Henwen\Project\ClassMapReduce("nars", "nars_log", "nars_mapreduce") ;

    // 2. 設定 Map function, Reduce function
    $obj->setMapReduceFunction($map, $reduce) ;

    // 3. 設定 Where 條件 // 指定特定日期範圍資料
    if (in_array($type, array("yesterday", "range"))) {
        $map_condition = array(
            "time" => array(
                '$gte' => $s_ut,
                '$lt' => $e_ut
            ),
            // "ip" => array('$ne' => 0)
        ) ;
        $obj->setQuery($map_condition) ;
    }

    // 4. 執行
    $obj->executeMapReduce() ;
