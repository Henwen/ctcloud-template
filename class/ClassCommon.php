<?php
    namespace Henwen\Common ;

    /**
    * static method
    */

    class ClassCommon
    {
        /**
         * 輸出 CSV 報表
         *
         * @param $filename String, 報表下載檔案名稱 Default report
         * @param $data Array, 要寫到 CSV 的資料陣列
         * @param $title Array, 資料標題, e.g. array("編號", "次數", "時間", ...)
         * @param $column Array, 需要的資料欄位, e.g. array("no", "total", "time", ...)
         */
        public static function outputCSV($filename = "report", $result = array(), $title = array(), $column = array())
        {
            // Header
            header("Content-Disposition: attachment;") ;

            // 建立 writer 物件
            $writer = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject()) ;

            // 設定 BOM：UTF8
            $writer->setOutputBOM(\League\Csv\Reader::BOM_UTF8) ;

            // 資料標題 和 資料欄位必須同時設定
            $flag = (! empty($title) && ! empty($column)) ? true : false ;

            // 有設定標題與欄位
            if ($flag) {
                $writer->insertOne($title) ;
            }
            else {
                $title_row = [] ;
                foreach ($result[0] as $key => $val) {
                    $title_row[] = $key ;
                }
                $writer->insertOne($title_row) ;
            }

            // 將每一筆資產寫入 csv 報表
            foreach ($result as $obj) {
                $push_data = [] ;
                // 有設定標題與欄位
                if ($flag) {
                    for ($i = 0 ; $i < count($column) ; $i++) {
                        $push_data[$i] = ! empty($obj[$column[$i]]) ? $obj[$column[$i]] : "" ;
                    }
                }
                else {
                    foreach ($obj as $key => $val) {
                        $push_data[] = $val ;
                    }
                }
                $writer->insertOne($push_data) ;
            }

            // 輸出報表
            $writer->output($filename.".csv") ;

            exit ;
        }

        // --------------------------------------------- //
        // ---------------- Data Output ---------------- //
        // --------------------------------------------- //
        /**
         * 參數檢驗錯誤, 輸出訊息至前台
         *
         * @param $msg String, 錯誤訊息, e.g. "使用者編號不能為空值"
         * @param $status Int, 錯誤代碼, e.g. 0
         */
        public static function failMessage($msg = "", $status = 0)
        {
            echo json_encode(array("status" => intval($status), "msg" => htmlentities($msg, ENT_QUOTES, "UTF-8")), JSON_UNESCAPED_UNICODE) ;
            exit ;
        }

        /**
         * 將陣列轉換成 JSON 字串輸出前台
         *
         * @param $data Array, 資料陣列
         */
        public static function dataOutput($data = array())
        {
            echo json_encode($data, JSON_UNESCAPED_UNICODE) ;
            exit ;
        }

        /**
         * 將陣列轉換成 JSON 字串輸出前台,
         *
         * @param $data Array, 資料陣列
         */
        public static function dataOutputWithStatus($data = array(), $msg = "")
        {
            if (! empty($data)) {
                $output = array(
                    "status" => 1,
                    "data" => $data
                ) ;
            }
            else {
                $output = array(
                    "status" => 0,
                    "data" => array(),
                    "msg" => $msg
                ) ;
            }
            echo json_encode($output, JSON_UNESCAPED_UNICODE) ;
            exit ;
        }
        // --------------------------------------------- //
        // ---------------- Data Output End ------------ //
        // --------------------------------------------- //
    }
