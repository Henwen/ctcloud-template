<?php
    namespace Henwen\Project ;

    class ClassMapReduce
    {
        /**
        * @var String: MongoDB IP
        */
        public $ip ;

        /**
        * @var Resource: MongoDB Connection Resource
        */
        public $con ;

        /**
        * @var String: MongoDB Database Name
        */
        public $database ;

        /**
        * @var String: MongoDB Collection Name, the Original Data Collection
        */
        public $target_collection ;

        /**
        * @var Resource: MongoDB Connection Database Resource
        */
        public $db ;

        /**
        * @var MongoCode String: Map-Reduce map function
        */
        public $map ;

        /**
        * @var MongoCode String: Map-Reduce reduce function
        */
        public $reduce ;

        /**
        * @var String: Mapreduce Condition Where Clause
        */
        public $query ;

        /**
        * @var String: Map-Reduce merge collection
        */
        public $output_collection ;

        /**
        * @var INT: Map-Reduce execution timeout
        */
        public $timeout ;

        /**
        * Constructor
        * @param $database 資料庫, e.g. win_nexus, nars
        * @param $target_collection Map-Reduce 目標集合, e.g. software_usage, nars_log
        * @param $output_collection String, 要輸出合併的集合，例如 mapreduce_su, mapreduce_pc, nars_mapreduce ...等
        */
        public function __construct($database = "", $target_collection = "", $output_collection = "")
        {
            // 資料庫、目標集合以及 Map-Reduce 輸出集合都必須存在
            if (empty($database) || empty($target_collection) || empty($output_collection)) {
                exit("datebase, target_collection and output_collection can not be empty.") ;
            }

            // MongoDB 連線
            $this->ip = IP ;
            $this->database = ! empty($database) ? $database : MONGODB_DEFAULT_DB ;
            $this->con = new \MongoClient("mongodb://$this->ip") ;

            // 檢查連線是否正常
            if (! $this->con) {
                exit("MongoDB 連線失敗") ;
            }

            $this->db = $this->con->{$this->database} ; // 指定資料庫
            $this->target_collection = $target_collection ; // 指定計算集合
            $this->output_collection = $output_collection ; // 指定 Map-Reduce 輸出集合
            $this->timeout = 3*24*60*60*1000 ; // Map-Reduce 執行時間設定 3 天
            $this->query = array() ; // Query 初始化
        }

        /**
        * Map, Reduce Function Setting
        * @param $str: Javascript Code
        */
        public function setMapReduceFunction($map, $reduce)
        {
            $this->map = new \MongoCode($map) ;
            $this->reduce = new \MongoCode($reduce) ;
        }

        /**
        * Map-Reduce 資料條件 Query 設定
        * @param $query Array
        */
        public function setQuery($query = array())
        {
            $this->query = $query ;
        }

        /**
        * Execute Map-Reduce
        * @return The Result of Map-Reduce
        */
        public function executeMapReduce()
        {
            try {
                if (empty($this->map->code) || empty($this->reduce->code)) {
                    throw new \Exception("Map, Reduce Javascsript Code can't be empty.\n") ;
                }

                echo "程式執行開始時間: ".date("Y-m-d H:i:s")."\n" ;

                // 下 Map-Reduce 指令
                $command_array = array(
                    "mapreduce" => $this->target_collection,
                    "map"       => $this->map,
                    "reduce"    => $this->reduce,
                    "out"       => array("merge" => $this->output_collection)
                ) ;

                // 如果有設定條件
                if (! empty($this->query)) {
                    // query 條件：要計算歸類的資料範圍
                    $command_array["query"] = $this->query ;
                }

                // runCommand: Execute Map-Reduce
                $result = $this->db->command($command_array, array("socketTimeoutMS" => $this->timeout)) ;

                // Print Result
                print_r($result) ;

                echo "程式執行結束時間: ".date("Y-m-d H:i:s")."\n----------------\n" ;
            }
            catch (\Exception $e) {
                exit($e->getMessage()) ;
            }
        }
    }
?>
