<?php
    namespace Henwen\Log ;

    /**
    * 負責註冊成功與錯誤處理器的類別
    * [註冊] PHP 最後錯誤處理器防線
    *   \Henwen\Log\ClassError::phpErrorHandlerRegister() ;
    * [註冊] MeekroDB 成功與錯誤處理器
    *   \Henwen\Log\ClassError::meekrodbHandlerRegister("test.log") ;
    *
    * [恢復預設錯誤處理器]
    *   \Henwen\Log\ClassError::phpErrorHandlerRestore() ;
    *   \Henwen\Log\ClassError::meekrodbHandlerRestore() ;
    */

    class ClassError
    {
        /**
         * Register, PHP Error Handler
         */
        public static function phpErrorHandlerRegister()
        {
            // PHP Error Handler
            set_error_handler( function ($errno, $errstr, $errfile, $errline ) {
                $m_obj = new \Henwen\Log\ClassErrorHandler("RUNTIME_ERROR.log") ;
                $m_obj->phpErrorHandler(array("error" => $errstr, "file" => $errfile, "line" => $errline)) ;
                \Henwen\Common\ClassCommon::failMessage("[程式執行錯誤] 請聯絡管理員", 0) ;
            }) ;
        }

        /**
         * Restore, PHP Error Handler
         */
        public static function phpErrorHandlerRestore()
        {
            // PHP Error Handler
            restore_error_handler() ;
        }

        /**
         * Register, PHP Error Handler & MeekroDB Handler
         *
         * @param $filename String, Log name, e.g. report, SYSTEM
         */
        public static function meekrodbHandlerRegister($filename = "report.log")
        {
            // MeekroDB Handler
            $meekrodb_obj = new \Henwen\Log\ClassErrorHandler($filename) ;
            \DB::$success_handler = array($meekrodb_obj, "meekrodbSuccessHandler") ;
            \DB::$error_handler = array($meekrodb_obj, "meekrodbErrorHandler") ;
        }

        /**
         * Restore, PHP Error Handler & MeekroDB Handler
         *
         * @param $filename String, Log name, e.g. ajax_user_access
         */
        public static function meekrodbHandlerRestore()
        {
            // MeekroDB
            \DB::$success_handler = false ;
            \DB::$error_handler = false ;
        }
    }
