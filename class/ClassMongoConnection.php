<?php
    namespace Henwen\Project ;

    /**
     * MongoDB Connection and Setting
     * 設定檔可另外寫成 MongoDB_config.php
     */
    class ClassMongoConnection {
        /**
         * @var 資料庫連線資源: new \MongoClient(...)
         */
        private $con ;
        
        /**
         * @var 資料庫
         */
        private $db ;

        /**
    	* 建構式
        * @param $ip, 連線主機，如果沒有設定則使用 config.php 內的全域變數
    	*/
        public function __construct($ip = "")
        {
            // 連線 ip
            $ip = ! empty($ip) ? $ip : MONGODB_IP ;

            // 進行 MongoDB 連線
            $this->con = new \MongoClient("mongodb://".$ip) ;
            if (! $this->con) {
                exit("連線失敗") ;
            }
        }

        /**
         * MongoDB 資料庫
         * @param $database_name String, 資料庫名稱
         * @return 資料庫集合連線資源
         */
        public function connectDB($database_name) {
            // 選擇資料庫
            $this->db = $this->con->{$database_name} ;
        }

        /**
         * MongoDB 集合
         * @param $collection_name String, 集合名稱
         * @return 資料庫集合連線資源
         */
        public function connectCollection($collection_name) {
            // 選擇集合並回傳連線資源
            return $this->db->{$collection_name} ;
        }
    }
?>
