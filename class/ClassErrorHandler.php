<?php
    namespace Henwen\Log ;
    /**
     * Meekrodb Error & Exceptioin Handler
     * 成功處理器、失敗處理器與其它管道的 Log
     * 使用方式 1 : Meekrodb
     *      $meekrodb_obj = new \Henwen\Project\ClassErrorHandler("test.log") ;
     *      \DB::$success_handler = array($meekrodb_obj, "meekrodbSuccessHandler") ;
     *      \DB::$error_handler = array($meekrodb_obj, "meekrodbErrorHandler") ;
     */
    class ClassErrorHandler {
        /**
         * Log 檔案名稱
         * @var String, e.g. test.log
         */
        private $filename ;

        /**
         * Log 物件
         * @var String, e.g. test.log
         */
        private $log ;

        /**
         * 建構式：初始化 Log 物件、檔名、Log 檔案串流
         * @param $filename String, Log 檔名, e.g. ajax_user_access
         */
        public function __construct($filename = "report.log")
        {
            // 設定 Log 檔名、Log記錄使用者辨識
            $this->filename = ! empty($filename) ? htmlentities($filename, ENT_QUOTES, "UTF-8") : "report.log" ;
            $who = ! empty($_SESSION["account"]) ? htmlentities($_SESSION["account"], ENT_QUOTES, "UTF-8") : "SYSTEM" ;

            // Log 物件
            $this->log = new \Monolog\Logger("[".$who."]");

            // 開啟檔案串流並加入串流至 Log 物件
            $stream = new \Monolog\Handler\StreamHandler(__DIR__."/../log/".$this->filename, \Monolog\Logger::INFO) ;
            $this->log->pushHandler($stream) ;
        }

        /**
         * 指令成功處理器：Meekrodb Library 執行成功之語句
         * @param $param Array, index: query & runtime
         */
        public function meekrodbSuccessHandler($params) {
            $this->log->addInfo($params["query"], array("runtime" => ($params["runtime"] * 1E-3)));
        }

        /**
         * 指令失敗處理器：Meekrodb Library 執行失敗之語句
         * @param $param Array, index: query & error
         */
        public function meekrodbErrorHandler($params) {
            $this->log->addInfo($params["query"], array("error" => $params["error"]));
            \Henwen\Common\ClassCommon::failMessage("[資料庫執行錯誤] 請聯絡管理員", 0) ;
        }

        /**
         * PHP 利用 set_error_handler() 捕捉到的執行期間其它錯誤
         * @param $param Array, index: error & file & line
         */
        public function phpErrorHandler($params) {
            $this->log->addInfo($params["error"], array("file" => $params["file"], "line" => $params["line"]));
        }

        /**
         * 手動記錄訊息至 Log 檔
         * @param $message String, 訊息字串
         */
        public function manualMessageHandler($message = "") {
            $this->log->addInfo(htmlentities($message, ENT_QUOTES, "UTF-8"));
        }

        /**
         * 解構式：移除 Handler 物件
         */
        public function __destruct()
        {
            unset($this) ;
        }
    }
?>
