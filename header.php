<div id="navbar" class="navbar navbar-default" style="color:#FFFFFF">
    <div class="navbar-container" id="navbar-container">
        <!-- #section:basics/sidebar.mobile.toggle -->
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only">Toggle sidebar</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
       <!-- /section:basics/sidebar.mobile.toggle -->

        <div class="navbar-header pull-left">
            <a class="navbar-brand"><img src="resources/images/logo.png" title="Logo"></a>
        </div>

        <?php
            // 有帳號 Session 變數
            if(! empty($_SESSION["account"])) {
        ?>
        <!-- Lougout -->
        <div class="navbar-buttons navbar-header pull-left" role="navigation">
            <ul class="nav ace-nav">
                <li class="light-blue">
                    <a data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
                        <span class="user-info">
                            <small>歡迎</small><?php echo htmlspecialchars($_SESSION["account"]) ; ?>
                        </span>
                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>
                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
                        <li class="divider"></li>
                        <li>
                            <a href="ajax_user_login.php?logout=1">
                                <i class="ace-icon fa fa-power-off"></i>
                                登出
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
       </div>
       <!-- Logout -->

       <?php
           }
       ?>

    </div>
</div>
