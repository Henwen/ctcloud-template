$(document).ready(function(){
    // JqGrid
    var grid_selector = "#grid-table" ;
    var pager_selector = "#grid-pager" ;

    // 全域變數
    var global_var = {
        download: null,
        condition_selected: []
    } ;

    // 點擊下載按鈕：依據上個查詢條件做軟體報表下載
    $("#btn-download").click(function(){
        if (! global_var.download) {
            alert("請先點選條件，做報表查詢。") ;
            return false ;
        }
        var url = "ajax/ajax_template.php?t="+Math.random() ;
        // url += "&start_date="+global_var.download.start_date ;
        location.href = url ;
    }) ;

    // 按下查詢按鈕
    $("#btn-search").click(function(){
        // 儲存查詢條件以便下載
        global_var.download = {
            start_date: $("#start_date").val(),
            end_date: $("#end_date").val(),
            act: $("#act").val(),
            group_no: $("#group_no").val()
        } ;
        // 重新讀取報表，主要是設定 datatype:json 會執行 url，而 page:1 是預設回到第 1 頁
        $(grid_selector).jqGrid('setGridParam',{datatype:'json', page:1}).trigger('reloadGrid') ;
    }) ;

    // --------------- JqGrid Table Report --------------- //

    //resize to fit page size
    $(window).on('resize.jqGrid', function () {
        $(grid_selector).jqGrid( 'setGridWidth', $(".page-content").width() );
    }) ;

    //resize on sidebar collapse/expand
    var parent_column = $(grid_selector).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
        if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
            //setTimeout is for webkit only to give time for DOM changes and then redraw!!!
            setTimeout(function() {
                $(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
            }, 0);
        }
    }) ;

    // JqGrid 主要函式：資料設定
    jQuery(grid_selector).jqGrid({
        caption: "統計結果", // 列表標題
        url: "ajax/ajax_template.php",
        // 依據表單條件，重新執行 JqGrid，傳給 url 參數。
        postData: {
            // t: function() { return Math.random() ; },
            // start_date: function() { return $("#start_date").val() ; },
            // end_date: function() { return $("#end_date").val() ; },
        },
        // 傳送參數類型為 GET
        mtype: 'GET',
        // 使用 local 是為了頁面載入時，不要做查詢的動作，等待 user 按下查詢鍵，再修改為 json，連結至 ajax_template.php 做查詢
        datatype: "local",
        // datatype: "json",
        colNames:["欄位1","欄位2","欄位3","欄位4"], // 列表欄位
        colModel:[
            {name:"col_1",index:"col_1", width:40, search:false, sortable:false},
            {name:"col_2",index:"col_2", width:200, search:false, sortable:false},
            {name:"col_3",index:"col_3", width:100, search:false, sortable:false},
            // 這是在 JqGrid 裡面做操連結的方式，可寫 HTML，例如 <button>。
            { name: "col_4_text", width: 100, sortable: false, search: false,
		        formatter: function (cellValue, options, rowObject) {
		            return "<a href='" + rowObject.col_4_href + "'>" + $.jgrid.htmlEncode(cellValue) + "</a>" ;
		        }
			}
        ],
        // sortname: "user_name", // 預設排序欄位
        // sortorder: "asc", // 排序方向 asc, desc
        rowNum:25, // 每頁數量
        rowList:[25, 50, 100,250,500,1000], // 可選擇的輸出數量 LIMIT
        pager : pager_selector, // 分頁 div
        ajaxRowOptions: { async: true },
        // 以下參數不要變動
        viewrecords: true, // 顯示目前筆數範圍及全部筆數
        altRows: true,
        toppager: true,
        multiboxonly: true,
        height: "auto",
        gridview: true,
        autoencode: true,
        /**
         * 讀取 url 完成後 Callback
         * @param 回傳資料，成功資料 ["rows", "records", "page", "total"]，失敗資料 ["status", "msg"]
         */
        loadComplete : function(data) {
            // 如果回傳有錯誤訊息，則顯示錯誤訊息
            // data = array("status" => 0, "msg" => "錯誤訊息內容")
            if (data.msg) {
                alert(data.msg) ;
                return ;
            }
            var table = this;
            setTimeout(function(){
                // JqGrid 分頁處理
                updatePagerIcons(table);
            }, 0);
        }
    });
    $(window).triggerHandler('resize.jqGrid');//trigger window resize to make the grid get the correct size

    //navButtons
    jQuery(grid_selector).jqGrid('navGrid',pager_selector,
        { 	//navbar options
            edit: false,
            editicon : 'ace-icon fa fa-pencil blue',
            add: false,
            addicon : 'ace-icon fa fa-plus-circle purple',
            del: false,
            delicon : 'ace-icon fa fa-trash-o red',
            search: false,
            searchicon : 'ace-icon fa fa-search orange',
            refresh: false,
            refreshicon : 'ace-icon fa fa-refresh green',
            view: true,
            viewicon : 'ace-icon fa fa-search-plus grey',
            cloneToTop: true
        },
        {
            //edit record form
        },
        {
            //new record form
        },
        {
            //delete record form
        },
        {
            //search form
            recreateForm: true,
            afterShowSearch: function(e){
                var form = $(e[0]);
                form.closest('.ui-jqdialog').find('.ui-jqdialog-title').wrap('<div class="widget-header" />')
                style_search_form(form);
            },
            afterRedraw: function(){
                style_search_filters($(this));
            },
            // multipleSearch: true, // 多重條件搜尋
            showQuery: false, // 顯示 SQL
            /**
            multipleGroup:true,
            */
        },
        {
            //view record form
            recreateForm: true,
            beforeShowForm: function(e){
                var form = $(e[0]);
                form.closest('.ui-jqdialog').find('.ui-jqdialog-title').wrap('<div class="widget-header" />')
            }
        }
    ) ;

    //replace icons with FontAwesome icons like above
    function updatePagerIcons(table) {
        var replacement =
        {
            'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
            'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
            'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
            'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
        };
        $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function(){
            var icon = $(this);
            var $class = $.trim(icon.attr('class').replace('ui-icon', ''));

            if($class in replacement) icon.attr('class', 'ui-icon '+replacement[$class]);
        })
    }

    $(document).one('ajaxloadstart.page', function(e) {
        $(grid_selector).jqGrid('GridUnload');
        $('.ui-jqdialog').remove();
    });

}) ;
