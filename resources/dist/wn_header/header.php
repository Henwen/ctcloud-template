<meta name="description" content="Static &amp; Dynamic Tables" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        
        	<link rel="stylesheet" href="wn_css/css.css" />

<!-- bootstrap & fontawesome -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" type="text/css" rel="stylesheet">
    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <link rel="stylesheet" href="css/jquery-ui.custom.min.css" />
    <link rel="stylesheet" href="css/datepicker.min.css">
    <link rel="stylesheet" href="css/ui.jqgrid.min.css">
    <!-- text fonts -->
    <link rel="stylesheet" href="css/ace-fonts.min.css">
    <!-- ace styles -->
    <link rel="stylesheet" href="css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style">
    <link rel="stylesheet" href="css/ace-skins.min.css" class="ace-main-stylesheet">
    <link rel="stylesheet" href="css/select2.min.css" />

    <!--[if lte IE 9]>
      <link rel="stylesheet" href="${ request.static_url('win_nexus.web:css/ace-part2.min.css') }" class="ace-main-stylesheet" />
    <![endif]-->
    <!--[if lte IE 9]>
      <link rel="stylesheet" href="${ request.static_url('win_nexus.web:css/ace-ie.min.css') }" />
    <![endif]-->
    <!-- inline styles related to this page -->
    <!-- ace settings handler -->
    <script src="js/ace-extra.min.js"></script>
    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
    <!--[if lte IE 8]>
    <script src="${ request.static_url('win_nexus.web:js/html5shiv.min.js') }"></script>
    <script src="${ request.static_url('win_nexus.web:js/respond.min.js') }"></script>
    <![endif]-->
     
 <!-- basic scripts -->
    <!--[if !IE]> -->
    <script src="js/jquery.min.js"></script>
    <!-- <![endif]-->
    <!--[if IE]>
    <script src ="${ request.static_url('win_nexus.web:js/jquery1x.min.js') }"></script>
    <![endif]-->
        <!-- page specific plugin scripts -->

    <!-- inline scripts related to this page -->

    
    <link rel="stylesheet" href="css/chosen.min.css" />
    <link rel="stylesheet" href="css/datepicker.min.css" />
    <link rel="stylesheet" href="css/bootstrap-timepicker.min.css" />
    <link rel="stylesheet" href="css/daterangepicker.min.css" />
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />
    <link rel="stylesheet" href="css/colorpicker.min.css" />
    <link rel="stylesheet" href="css/bootstrap-duallistbox.min.css" />
    <link rel="stylesheet" href="css/bootstrap-multiselect.min.css" />
    
<!--[if !IE]> -->
    <script type="text/javascript">
      window.jQuery || document.write("<script src='js/jquery.min.js'>"+"<"+"/script>");
    </script>

    <!-- <![endif]-->

    <!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
    <script type="text/javascript">
      if('ontouchstart' in document.documentElement) document.write("<script src='js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>
    <script src="js/bootstrap.min.js"></script>

    <!-- page specific plugin scripts -->

    <!--[if lte IE 8]>
      <script src="assets/js/excanvas.js"></script>
    <![endif]-->

    <script src="js/fuelux/fuelux.wizard.min.js"></script>
    <script src="js/jquery.validate.mod.js"></script>
    <script src="js/additional-methods.min.js"></script>
    <script src="js/bootbox.min.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script src="js/select2.min.js"></script>



    <script src="js/ace-elements.min.js"></script>
    <script src="js/ace.min.js"></script>

    <script src="js/date-time/bootstrap-datepicker.min.js"></script>
    <script src="js/jquery.bootstrap-duallistbox.min.js"></script>
    <script src="js/dataTables/jquery.dataTables.min.js"></script>
    <script src="js/dataTables/jquery.dataTables.bootstrap.min.js"></script>
    <script src="js/dataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
    <script src="js/dataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
    <script src="js/chosen.jquery.min.js"></script>
    
    <script type="text/javascript">
            jQuery(function($) {
            
                $('[data-rel=tooltip]').tooltip();
            
                var $validation = false;

                
                //documentation : http://docs.jquery.com/Plugins/Validation/validate
            
                $('#validation-form').validate({
                    errorElement: 'div',
                    errorClass: 'help-block',
                    focusInvalid: false,
                    ignore: "",
                    rules: {
                        
                        "safe_file_name[]": {
                            required: true
                        },
                        "safe_web_name": {
                            required: true
                        },
                        "safe_web_url": {
                            required: true
                        },
                        "safe_rule_name": {
                            required: true
                        }


                    },
            
                    messages: {
                        
                        "safe_file_name[]": "請填寫路徑名稱",
                        "safe_web_name": "請填寫名稱",
                        "safe_web_url": "請填寫網址",
                        "safe_rule_name": "請填寫原則名稱"
                        
                    },
            
            
                    highlight: function (e) {
                        $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
                    },
            
                    success: function (e) {
                        $(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
                        $(e).remove();
                    }
                });
            
            })
        </script>





    <script type="text/javascript">
         
      jQuery(function($) {
        
      
        //datepicker plugin
        //link
        $('.date-picker').datepicker({
          autoclose: true,
          todayHighlight: true
        })
        //show datepicker when clicking on the icon
        .next().on(ace.click_event, function(){
          $(this).prev().focus();
        });
      
        //or change it into a date range picker
        $('.input-daterange').datepicker({autoclose:true});

      
      });
	  
	  
    </script>

    <!-- the following scripts are used in demo only for onpage help and you don't need them -->